import setuptools

with open("README.md", "r", encoding="utf-8") as file_readme:
    readme = file_readme.read()

setuptools.setup(
    version="1.01",
    install_requires=['numpy'],

    description="Help calculate the electric intensity and potential in vacuum.",

    name='electrostaticvacuum',
    long_description=readme,
    long_description_content_type="text/markdown",
    url="https://gitee.com/yueyinqiu5990/electrostaticvacuum.git",
    packages=setuptools.find_packages(),
    license="MIT"
)
