import math
import unittest
from electrostaticvacuum import Vector


class VectorTest(unittest.TestCase):
    def test_x_y(self):
        o = Vector(0, 0)
        th_fo = Vector(3, 4)
        flt = Vector(213.1231, -123.12312)
        self.assertAlmostEqual(0, o.x)
        self.assertAlmostEqual(0, o.y)
        self.assertAlmostEqual(3, th_fo.x)
        self.assertAlmostEqual(4, th_fo.y)
        self.assertAlmostEqual(213.1231, flt.x)
        self.assertAlmostEqual(-123.12312, flt.y)

    def test_distance(self):
        o = Vector(0, 0)
        th_fo = Vector(3, 4)
        flt = Vector(213.1231, -123.12312)
        self.assertAlmostEqual(5, o.get_distance_between(th_fo))
        self.assertAlmostEqual(246.1315876, o.get_distance_between(flt), delta=1E-6)
        self.assertAlmostEqual(245.5850256, th_fo.get_distance_between(flt), delta=1E-6)
        self.assertAlmostEqual(5, th_fo.get_distance_between(o))
        self.assertAlmostEqual(246.1315876, flt.get_distance_between(o), delta=1E-6)
        self.assertAlmostEqual(245.5850256, flt.get_distance_between(th_fo), delta=1E-6)

    def test_magic(self):
        o = Vector(0, 0)
        th_fo = Vector(3, 4)

        self.assertTrue(th_fo == Vector(3, 4))
        self.assertFalse(th_fo == o)

        self.assertEqual(th_fo.__hash__(), Vector(3, 4).__hash__())
        self.assertNotEqual(th_fo.__hash__(), o.__hash__())

        self.assertEqual("(3.0, 4.0)", th_fo.__repr__())

        self.assertEqual(True, o.__bool__())
        self.assertEqual(False, th_fo.__bool__())

    def test_polar(self):
        th_fo = Vector(3, 4)
        th_fo_like = Vector.create_point_with_polar(5, math.atan(4 / 3))
        self.assertAlmostEqual(th_fo.x, th_fo_like.x)
        self.assertAlmostEqual(th_fo.y, th_fo_like.y)


if __name__ == '__main__':
    unittest.main()
