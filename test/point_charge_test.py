import math
import unittest
from electrostaticvacuum import Vector, PointCharge


class PointChargeTest(unittest.TestCase):
    def test_x_y_charge(self):
        o = PointCharge(Vector(0, 0), 1)
        th_fo = PointCharge(Vector(3, 4), -2)
        flt = PointCharge(Vector(213.1231, -123.12312), 2.31)
        self.assertAlmostEqual(0, o.x)
        self.assertAlmostEqual(0, o.y)
        self.assertAlmostEqual(1, o.charge)
        self.assertAlmostEqual(3, th_fo.x)
        self.assertAlmostEqual(4, th_fo.y)
        self.assertAlmostEqual(-2, th_fo.charge)
        self.assertAlmostEqual(213.1231, flt.x)
        self.assertAlmostEqual(-123.12312, flt.y)
        self.assertAlmostEqual(2.31, flt.charge)

    def test_distance(self):
        o = PointCharge(Vector(0, 0), 1)
        th_fo = PointCharge(Vector(3, 4), -2)
        flt = PointCharge(Vector(213.1231, -123.12312), 2.31)
        self.assertAlmostEqual(5, o.get_distance_between(th_fo))
        self.assertAlmostEqual(246.1315876, o.get_distance_between(flt), delta=1E-6)
        self.assertAlmostEqual(245.5850256, th_fo.get_distance_between(flt), delta=1E-6)
        self.assertAlmostEqual(5, th_fo.get_distance_between(o))
        self.assertAlmostEqual(246.1315876, flt.get_distance_between(o), delta=1E-6)
        self.assertAlmostEqual(245.5850256, flt.get_distance_between(th_fo), delta=1E-6)

    def test_magic(self):
        th_fo = PointCharge(Vector(3, 4), -2)

        self.assertEqual("<a point charge at (3.0, 4.0) with -2.0 unit quantities>", th_fo.__str__())


if __name__ == '__main__':
    unittest.main()
